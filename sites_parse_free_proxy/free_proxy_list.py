from bs4 import BeautifulSoup as bs
from enum import Enum
import requests


class RowDataOne(Enum):
    IP = 0
    PORT = 1
    CODE_COUNTRY = 2
    COUNTRY = 3
    LEVEL_ANONYMITY = 4
    CAN_GOOGLE = 5
    CAN_HTTPS = 6


class LevelAnonymityOne(Enum):
    MAX = "elite proxy"
    MIDDLE = "anonymous"
    LOW = ""


def _get_from_one(list_ignoring_country, list_certain_country, list_anonymity, can_google, can_https):
    url = "https://free-proxy-list.net/"
    soup = bs(requests.get(url).content, "html.parser")
    proxies = []
    for row in soup.find("table", attrs={"class": "table-striped"}).find_all("tr")[1:]:
        tds = row.find_all("td")
        try:
            ip = tds[RowDataOne.IP.value].text.strip()
            port = tds[RowDataOne.PORT.value].text.strip()
            code_country = tds[RowDataOne.CODE_COUNTRY.value].text.strip()
            proxy_level_anonymity = tds[RowDataOne.LEVEL_ANONYMITY.value].text.strip()
            proxy_can_google = tds[RowDataOne.CAN_GOOGLE.value].text.strip()
            proxy_can_https = tds[RowDataOne.CAN_HTTPS.value].text.strip()
            
            host = f"{ip}:{port}"
            
            if list_ignoring_country and code_country in list_ignoring_country:
                continue
            if list_certain_country and code_country not in list_certain_country:
                continue
            if can_google is True and proxy_can_google == "no":
                continue
            if can_https is True and proxy_can_https == "no":
                continue
            if list_anonymity and LevelAnonymityOne("proxy_level_anonymity") not in list_anonymity:
                continue
            
            proxies.append(host)
        
        except IndexError:
            continue
    return proxies


def _get_from_two(list_ignoring_country, list_certain_country, list_anonymity, can_google, can_https):
    url = "https://www.sslproxies.org/"
    soup = bs(requests.get(url).content, "html.parser")
    proxies = []
    for row in soup.find("table", attrs={"class": "table-striped"}).find_all("tr")[1:]:
        tds = row.find_all("td")
        try:
            ip = tds[RowDataOne.IP.value].text.strip()
            port = tds[RowDataOne.PORT.value].text.strip()
            code_country = tds[RowDataOne.CODE_COUNTRY.value].text.strip()
            proxy_level_anonymity = tds[RowDataOne.LEVEL_ANONYMITY.value].text.strip()
            proxy_can_google = tds[RowDataOne.CAN_GOOGLE.value].text.strip()
            proxy_can_https = tds[RowDataOne.CAN_HTTPS.value].text.strip()
            
            host = f"{ip}:{port}"
            
            if list_ignoring_country and code_country in list_ignoring_country:
                continue
            if list_certain_country and code_country not in list_certain_country:
                continue
            if can_google is True and proxy_can_google == "no":
                continue
            if can_https is True and proxy_can_https == "no":
                continue
            if list_anonymity and LevelAnonymityOne("proxy_level_anonymity") not in list_anonymity:
                continue
            
            proxies.append(host)
        
        except IndexError:
            continue
    return proxies


def get_ip_for_check(
        list_ignoring_country=None,
        list_certain_country=None,
        list_level_anonymity=None,
        must_can_google=False,
        must_can_https=False):
    
    list_ip_one = _get_from_one(
        list_ignoring_country=list_ignoring_country,
        list_certain_country=list_certain_country,
        list_anonymity=list_level_anonymity,
        can_google=must_can_google,
        can_https=must_can_https
    )
    list_ip_two = _get_from_two(
        list_ignoring_country=list_ignoring_country,
        list_certain_country=list_certain_country,
        list_anonymity=list_level_anonymity,
        can_google=must_can_google,
        can_https=must_can_https
    )
    
    return list_ip_one + list_ip_two