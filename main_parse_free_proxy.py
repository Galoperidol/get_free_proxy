from sites_parse_free_proxy import free_proxy_list

"""
todo
Free Proxy List (https://free-proxy-list.net/) - done
SSL Proxy (https://www.sslproxies.org/) - done
Proxy Nova (https://www.proxynova.com/)
Proxy Scrape (https://proxyscrape.com/)
Hide My Name (https://hidemy.name/en/proxy-list/)
Proxy List+ (https://list.proxylistplus.com/)
Spys.one (http://spys.one/)
"""

def get_proxy_list(
        list_ignoring_country=None,
        list_certain_country=None,
        list_level_anonymity=None,
        must_can_google=False,
        must_can_https=False):
    
    list_proxy_ip_from_all_sites = []
    list_proxy_ip_from_all_sites.append(free_proxy_list.get_ip_for_check(
        list_ignoring_country=list_ignoring_country,
        list_certain_country=list_certain_country,
        list_level_anonymity=list_level_anonymity,
        must_can_google=must_can_google,
        must_can_https=must_can_https
    ))
    returned_list = []
    
    for proxy_from_each_site in list_proxy_ip_from_all_sites:
        returned_list += proxy_from_each_site
        
    return returned_list
