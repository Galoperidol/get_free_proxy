"""
https://waksoft.susu.ru/2021/04/15/kak-v-python-ispolzovat-proksi-dlya-podmeny-ip%E2%80%91adresov/
"""
import asyncio
import aiohttp
from main_parse_free_proxy import get_proxy_list


async def make_query(session, url, proxy_host):
    proxy_url = f"http://{proxy_host}"
    try:
        async with session.get(url, proxy=proxy_url) as response:
            result = await response.json()
            return proxy_host
    except Exception as e:
        return 0


async def main(url, list_proxy_data):
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(total=4)) as session:
        tasks = []
        for proxy_url in list_proxy_data:
            tasks.append(make_query(session, url, proxy_url))
        responses = await asyncio.gather(*tasks)
    
    return responses


def get_checked_ip(url, list_proxy_data):
    loop = asyncio.get_event_loop()
    data = loop.run_until_complete(
        main(url, list_proxy_data)
    )
    return [ip for ip in list(set(data)) if ip != 0]


free_proxies = get_proxy_list(list_ignoring_country=['RU'])
clear_proxy = get_checked_ip("https://httpbin.org/ip", free_proxies)
print(f"Вот список чистых IP: {clear_proxy}")
